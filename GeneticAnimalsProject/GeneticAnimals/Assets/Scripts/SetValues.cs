﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetValues : MonoBehaviour
{

    public Slider sliderRich;
    public Slider sliderPoor;
    public Slider sliderDesert;
    public Slider sliderGlacial;
    public Slider sliderJungle;
    public Slider sliderTundra;

    public SO_FloatVariable richValue;
    public SO_FloatVariable poorValue;
    public SO_FloatVariable desertValue;
    public SO_FloatVariable glacialValue;
    public SO_FloatVariable jungleValue;
    public SO_FloatVariable tundraValue;

    public void SetMapValues()
    {
        richValue.floatVariable = sliderRich.value;
        poorValue.floatVariable = sliderPoor.value;
        desertValue.floatVariable = sliderDesert.value;
        glacialValue.floatVariable = sliderGlacial.value;
        jungleValue.floatVariable = sliderJungle.value;
        tundraValue.floatVariable = sliderTundra.value;
    }

}
