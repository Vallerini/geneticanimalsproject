﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TickCounter : MonoBehaviour
{

    public int tickSurvived = 0;
    public int lastBrain = 0;
    public GameEvent newBarInHUD;
    public SO_IntVariable brainDevInt;

	public void AddTick()
    {
        tickSurvived++;
        GetComponent<Text>().text = "Tick Survived: " + tickSurvived + "\nLast Brain: " + lastBrain;
    }

    public void ResetTicks()
    {
        lastBrain = tickSurvived;
        tickSurvived = 0;
        GetComponent<Text>().text = "Tick Survived: " + tickSurvived + "\nLast Brain: " + lastBrain;
        brainDevInt.intVariable = lastBrain;
        newBarInHUD.Raise();
    }

}
