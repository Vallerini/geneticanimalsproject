﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeButtons : MonoBehaviour
{
    public SO_TileClassVariable classSelected;
    public SO_FoodOrTemp foodOrTemp;
    public SO_FloatVariable value;
    public SO_GameValues gameValues;
    public SO_BoolVariable isClassChanger;
    public GameEvent SelectedBiomeButtonEv;

    public void SelectedRich()
    {
        classSelected.classVariable = TileStats.TileClass.Rich;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedPoor()
    {
        classSelected.classVariable = TileStats.TileClass.Poor;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedDesert()
    {
        classSelected.classVariable = TileStats.TileClass.Desert;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedGlacial()
    {
        classSelected.classVariable = TileStats.TileClass.Glacial;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedJungle()
    {
        classSelected.classVariable = TileStats.TileClass.Jungle;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedTundra()
    {
        classSelected.classVariable = TileStats.TileClass.Tundra;
        isClassChanger.boolVariable = true;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedFoodMax()
    {
        foodOrTemp.variable = SO_FoodOrTemp.FoodOrTemp.FOOD;
        value.floatVariable = gameValues.maxFood;
        isClassChanger.boolVariable = false;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedFoodMin()
    {
        foodOrTemp.variable = SO_FoodOrTemp.FoodOrTemp.FOOD;
        value.floatVariable = gameValues.minFood;
        isClassChanger.boolVariable = false;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedTempMax()
    {
        foodOrTemp.variable = SO_FoodOrTemp.FoodOrTemp.TEMP;
        value.floatVariable = gameValues.maxTemp;
        isClassChanger.boolVariable = false;
        SelectedBiomeButtonEv.Raise();
    }

    public void SelectedTempMin()
    {
        foodOrTemp.variable = SO_FoodOrTemp.FoodOrTemp.TEMP;
        value.floatVariable = gameValues.minTemp;
        isClassChanger.boolVariable = false;
        SelectedBiomeButtonEv.Raise();
    }

}
