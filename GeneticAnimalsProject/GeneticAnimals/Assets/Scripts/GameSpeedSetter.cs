﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class GameSpeedSetter : MonoBehaviour
{
    [SerializeField] private SO_GameValues gameValues = null;
    [SerializeField] private Button increaseButton = null;
    [SerializeField] private Button decreaseButton = null;
    [SerializeField] private Text speedText = null;

    private void Start()
    {
        increaseButton.onClick.AddListener(IncreaseSpeed);
        decreaseButton.onClick.AddListener(DecreaseSpeed);
        UpdateText();
    }

    private void IncreaseSpeed()
    {
        if (gameValues.tickSpeed < 20000)
        {
            gameValues.tickSpeed++;
            UpdateText();
        }
    }

    private void DecreaseSpeed()
    {
        if (gameValues.tickSpeed > 1)
        {
            gameValues.tickSpeed--;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        speedText.text = gameValues.tickSpeed.ToString(); 
    }
}
