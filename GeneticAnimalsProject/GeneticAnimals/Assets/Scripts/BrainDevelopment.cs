﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainDevelopment : MonoBehaviour
{

    public GameObject bar;
    public SO_IntVariable sizeY;
    public int maxBars = 20;

    private List<GameObject> bars = new List<GameObject>();

    public void AddBar()
    {
        var spawned = Instantiate(bar, transform);
        spawned.transform.localScale = sizeY.intVariable > 100 ? new Vector3(1, 100, 1) : new Vector3(1, sizeY.intVariable, 1);
        bars.Add(spawned);
        CancelOld();
    }

    private void CancelOld()
    {
        while(bars.Count > maxBars)
        {
            Destroy(bars[0]);
            bars.RemoveAt(0);
        }
    }
}
