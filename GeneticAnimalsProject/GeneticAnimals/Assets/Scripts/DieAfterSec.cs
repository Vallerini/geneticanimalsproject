﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieAfterSec : MonoBehaviour {

    public float sec = 2;

    float t = 0;

    private void Update()
    {
        t += Time.deltaTime;
        if (t > sec)
            Destroy(gameObject);
    }

}
