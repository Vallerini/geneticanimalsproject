﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycasterGenerator : MonoBehaviour
{

    public SO_GameMapVal SO_gameMapVal;
    public GameObject animalePadre;

    Animal padreScript;
    public List<Vector3> EmitterPositions = new List<Vector3>();

    private void Start()
    {
        padreScript = animalePadre.GetComponent<Animal>();
        FindTiles();
    }

    public void FindTiles()
    {
        EmitterPositions.Clear();
        float vert = SO_gameMapVal.distTileVert;
        float hor = SO_gameMapVal.distTileHor;
        float x = transform.position.x;
        float y = transform.position.y;
        float z = transform.position.z;
        //le 19 posizioni puntate dall'animale
        EmitterPositions.Add(new Vector3(x, y, z));
        EmitterPositions.Add(new Vector3(x, y, z + vert));
        EmitterPositions.Add(new Vector3(x + hor, y, z + (vert / 2)));
        EmitterPositions.Add(new Vector3(x + hor, y, z - (vert / 2)));
        EmitterPositions.Add(new Vector3(x, y, z - vert));
        EmitterPositions.Add(new Vector3(x - hor, y, z - (vert / 2)));
        EmitterPositions.Add(new Vector3(x - hor, y, z + (vert / 2)));
        EmitterPositions.Add(new Vector3(x - hor, y, z + vert + (vert / 2)));
        EmitterPositions.Add(new Vector3(x, y, z + (vert * 2)));
        EmitterPositions.Add(new Vector3(x + hor, y, z + vert + (vert / 2)));
        EmitterPositions.Add(new Vector3(x + (hor * 2), y, z + vert));
        EmitterPositions.Add(new Vector3(x + (hor * 2), y, z));
        EmitterPositions.Add(new Vector3(x + (hor * 2), y, z - vert));
        EmitterPositions.Add(new Vector3(x + hor, y, z - vert - (vert / 2)));
        EmitterPositions.Add(new Vector3(x, y, z - (vert * 2)));
        EmitterPositions.Add(new Vector3(x - hor, y, z - vert - (vert / 2)));
        EmitterPositions.Add(new Vector3(x - (hor * 2), y, z - vert));
        EmitterPositions.Add(new Vector3(x - (hor * 2), y, z));
        EmitterPositions.Add(new Vector3(x - (hor * 2), y, z + vert));
        padreScript.EmitterPositions = EmitterPositions;
    }

}
