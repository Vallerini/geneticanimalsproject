﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockInput : MonoBehaviour {

    public KeyCode input;
    public GameEvent preTickEvent;
    public GameEvent tickEvent;
    public GameEvent postTickEvent;
    public SO_GameValues SO_gameValues;

    float t = 0;

    private void OnEnable()
    {
        t = 0;
    }

    void Update()
    {
        t += Time.deltaTime;
        if (Input.GetKeyDown(input) && t > 1 / SO_gameValues.tickSpeed)
        {
            RaiseTickEvents();
            t = 0;
        }
    }

    void RaiseTickEvents()
    {
        preTickEvent.Raise();
        tickEvent.Raise();
        StartCoroutine(postTickCall());
    }

    IEnumerator postTickCall()
    {
        yield return new WaitForSeconds(1 / SO_gameValues.tickSpeed);
        postTickEvent.Raise();
    }
}
