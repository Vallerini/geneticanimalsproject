﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClock : MonoBehaviour {

    public GameEvent preTickEvent;
    public GameEvent tickEvent;
    public GameEvent postTickEvent;
    public SO_GameValues SO_gameValues;

    public bool dying = false;

    float t = 0;

    void Update()
    {
        t -= Time.deltaTime;

        if(t <= 0 && !dying)
        {
            t = 1 / SO_gameValues.tickSpeed;
            preTickEvent.Raise();
            tickEvent.Raise();
            StartCoroutine(postTickCall());
        }
    }

    IEnumerator postTickCall()
    {
        yield return new WaitForSeconds(1 / SO_gameValues.tickSpeed);
        postTickEvent.Raise();
    }
}
