﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_FloatVariable", menuName = "FloatVariable")]
public class SO_FloatVariable : ScriptableObject
{

    public float floatVariable = 0;

}
