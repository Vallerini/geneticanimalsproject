﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_TileClassVariable", menuName = "TileClassVariable")]
public class SO_TileClassVariable : ScriptableObject
{

    public TileStats.TileClass classVariable = TileStats.TileClass.Rich;

}
