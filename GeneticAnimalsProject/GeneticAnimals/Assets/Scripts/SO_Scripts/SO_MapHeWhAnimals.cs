﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_MapHeWhAnimals", menuName = "MapHeWhAnimals")]
public class SO_MapHeWhAnimals : ScriptableObject
{

    public int height = 20;
    public int width = 20;
    public int animals = 20;

}
