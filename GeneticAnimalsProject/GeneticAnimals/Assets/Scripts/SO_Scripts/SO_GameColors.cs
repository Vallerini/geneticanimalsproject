﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_GameColors", menuName = "GameColorSet")]
public class SO_GameColors : ScriptableObject {

    public Color temperatureMax;
    public Color temperatureMin;
    public Color foodMax;
    public Color foodMin;

}
