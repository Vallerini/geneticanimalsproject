﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_GameMapVal", menuName = "GameMapVal")]
public class SO_GameMapVal : ScriptableObject {

    public float distTileVert = 1.7f;
    public float distTileHor = 1.5f;
    public float animalsHeight = 1.55f;

}
