﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_FoodOrTemp", menuName = "FoodOrTemp")]
public class SO_FoodOrTemp : ScriptableObject
{
    public enum FoodOrTemp
    {
        FOOD,
        TEMP,
    }

    public FoodOrTemp variable = FoodOrTemp.FOOD;

}
